import React, { useState, useEffect } from 'react';
import TodoForm from './TodoForm';
import Todo from './Todo';
import axios from "axios";

function TodoList() {


    const [todos, setTodos] = useState([]);

    const LOCAL_STORAGE_KEY = "react-do-list-todos";

    useEffect(() => {
        const storageTodos = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));

        if (storageTodos) {
            setTodos(storageTodos)
        }
    }, []);

    useEffect(() => {
        localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(todos));
    }, [todos]);

    const addTodo = todo => { //agrega
        if (!todo.text || /^\s*$/.test(todo.text)) {
            return
        }

        const newTodos = [todo, ...todos];

        setTodos(newTodos);


        /*export default {
        created() {
            axios.get("https://jsonplaceholder.typicode.com/todos/1").then((result) => {
            console.log(result.data);
            })
        }
        };*/
        axios.post("http://3.86.188.138", todo)
        .then(data => {
            console.log(...todos);
        });

        /*axios({
            method: 'post',
            url: 'https://jsonplaceholder.typicode.com/posts',
            data: {
              todo
            }
          })*/
        //console.log(...todos);
    };

    const updateTodo = (todoId, newValue) => { // actualiza
        if (!newValue.text || /^\s*$/.test(newValue.text)) {
            return
        }
        const edit = { id: newValue, text: newValue.text };
        axios.patch(`http://3.86.188.138/${todoId}`,edit).then(res => {
            setTodos(prev => prev.map(item => (item.id === todoId ? newValue : item)));
            console.log(res);
        }).catch(err => console.log(err));

        setTodos(prev => prev.map(item => (item.id === todoId ? newValue : item)));


        /*axios({
            method: 'post',
            url: 'https://jsonplaceholder.typicode.com/posts',
            data: {
              todo
            }
          })*/
    }

    const removeTodo = id => { ///borra to do
        const removeArr = [...todos].filter(todo => todo.id !== id);

        axios.delete(`http://3.86.188.138/${id}`)
            .then(res => {
            console.log(res);
            console.log(res.data);
            setTodos(removeArr);
        })

        setTodos(removeArr);
    };

    const completeTodo = id => {
        let updatedTodos = todos.map(todo => {
            if (todo.id === id) {
                todo.isComplete = !todo.isComplete
            }
            return todo;
        })
        setTodos(updatedTodos);
    };

    return (
        <div className="todo-context">
            <h1>What's the Plan for Today?</h1>
            <TodoForm onSubmit={addTodo} />
            <Todo todos={todos} completeTodo={completeTodo} removeTodo={removeTodo} updateTodo={updateTodo} />
        </div>
    );
}

export default TodoList;
