# Proyecto del primer parcial

El proyecto del primer parcial consiste en implementar la siguiente arquitectura de una aplicación web de un TODO List debe cumplir los puntos descritos al final:

![Image text](arquitectura.png)

1. La arquitectura muestra diversas instancias de EC2 y una de S3 que deben configurarse.

2. Los accesos a los servidores debe ser mediante el servidor bastion.

3. Las dos maquinas EC2 que usan docker deben tener un compose que ejecute un Ngnix y dos instancias de la aplicación dockerizadas.

4. Como entregable se deben entregar todos los archivos de configuración (en gitlab) y generar una presentación donde documenten paso a paso cada etapa del proyecto incluyendo el diagrama de red anterior colocando las IP's o DNS colocadas en cada conexión.

## Comenzando 🚀

Esta aplicación es para alojar en S3

## Autores

Ana Marina Ortiz 329575

Melissa García Mendoza 334259

## Grupo
Cloud Computing 8CC2

## Todos los creditos a:

* [Github](https://github.com/mmeii/react-todo-list)


## License

  Copyright (c) Mengmei Tu. All rights reserved.

  Licensed under the [MIT](LICENSE) license.
